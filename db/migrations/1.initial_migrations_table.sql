CREATE TABLE `goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `cost` FLOAT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `state` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_goods` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `order_id` int(11) NOT NULL,
    `goods_id` int(11) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`order_id`)
      REFERENCES `order`(`id`)
      ON DELETE CASCADE,
      FOREIGN KEY (`goods_id`)
      REFERENCES `goods`(`id`)
      ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

