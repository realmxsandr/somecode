<?php

namespace AllTools\Controller;

use AllTools\Core\App;
use AllTools\Core\AppUser;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractController
{
    /** @var Request */
    protected $request;
    /** @var App */
    protected $app;

    /**
     * AbstractController constructor.
     * @param Request $request
     */
    public function __construct(Request $request, App $app)
    {
        $this->request = $request;
        $this->app = $app;
    }

    /**
     * Returns a service by $serviceClassName
     *
     * @param string $serviceClassName
     * @return object
     */
    protected function get(string $serviceClassName): object
    {
        return $this->app->getService($serviceClassName);
    }

    /**
     * Returns current user
     *
     * @return AppUser
     */
    public function getUser(): AppUser
    {
        return $this->app->getUser();
    }
}