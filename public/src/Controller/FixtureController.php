<?php

namespace AllTools\Controller;

use AllTools\Fixtures\GoodsLoadFixture;
use Symfony\Component\HttpFoundation\JsonResponse;

class FixtureController extends AbstractController
{
    /**
     * Loads 20 test goods
     */
    public function loadFixtures()
    {
        $goodsLoadFixture = new GoodsLoadFixture();
        $goodsLoadFixture->execute();

        return new JsonResponse('All was tables purged. Fixtures reloaded.');
    }
}