<?php

namespace AllTools\Controller;

use AllTools\Repository\GoodsRepository;
use Symfony\Component\HttpFoundation\JsonResponse;

class GoodsController extends AbstractController
{
    /**
     * Returns all available goods
     *
     * @return JsonResponse
     */
    public function getAll()
    {
        $goodsRepository = new GoodsRepository();
        $goods = \iterator_to_array($goodsRepository->getAll());

        return new JsonResponse($goods);
    }
}