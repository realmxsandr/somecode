<?php

namespace AllTools\Controller;

use AllTools\Entity\Order;
use AllTools\Repository\Exception\RepositoryException;
use AllTools\Repository\OrderRepository;
use AllTools\Service\OrderManager\Exception\OrderManagerException;
use AllTools\Service\OrderManager\OrderManager;
use AllTools\Service\OrderStates\Exception\OrderStateException;
use Symfony\Component\HttpFoundation\JsonResponse;

class OrderController extends AbstractController
{
    /**
     * Creates order with goods from goods GET parameter
     */
    public function create()
    {
        try {
            $orderGoodsIds = \explode(',', $this->request->get('goods'));
            /** @var OrderManager $orderManager */
            $orderManager = $this->get(OrderManager::class);
            $order = $orderManager->createOrder($this->getUser(), $orderGoodsIds);
        } catch (\Throwable $throwable) {
            return new JsonResponse($throwable->getMessage());
        }

        return new JsonResponse($order->getId());
    }

    /**
     * Charge order with id order_id and amount payment_amount GET parameters
     * @throws OrderStateException
     */
    public function charge()
    {
        try {
            /** @var OrderManager $orderManager */
            $orderManager = $this->get(OrderManager::class);
            $orderRepository = new OrderRepository();
            $orderId = $this->request->get('order_id');
            $paymentAmount = $this->request->get('payment_amount');
            /** @var Order $chargeableOrder */
            $chargeableOrder = $orderRepository->get($orderId);
            if(is_null($chargeableOrder)) {
                throw OrderManagerException::generateUnknownOrder();
            }
            $orderManager->chargeOrder($chargeableOrder, $paymentAmount);
        } catch (\Throwable $throwable) {
            return new JsonResponse($throwable->getMessage());
        }

        return new JsonResponse('Order ' . $chargeableOrder->getId() . ' successfully charged');
    }
}