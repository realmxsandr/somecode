<?php

namespace AllTools\Core;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class App
{
    /** @var App */
    private static $instance = null;
    /** @var ServiceLoader */
    private $serviceLoader;
    /** @var RoutingLoader */
    private $routingLoader;
    /** @var AppUser */
    private $user;
    /** @var Request */
    private $request;

    /**
     * App constructor.
     */
    private function __construct()
    {
        $this->serviceLoader = ServiceLoader::getInstance();
        $this->routingLoader = RoutingLoader::getInstance();
        $this->request = Request::createFromGlobals();
        $this->user = (new AuthorizationProvider())->getUser($this->request);
    }

    /**
     * @return App
     */
    public static function getInstance()
    {
        if (self::$instance == null)
        {
            self::$instance = new App();
        }

        return self::$instance;
    }

    /**
     * Return $classname class instance
     *
     * @param string $classname
     * @return object
     */
    public function getService(string $classname)
    {
        return $this->serviceLoader->getService($classname);
    }

    /**
     * Process process app request
     * @return void
     */
    public function processAppRequest(): void
    {
        $matchedControllerAndActionData = $this->routingLoader->getMatchedControllerAndAction();
        $controller = $matchedControllerAndActionData['_controller'];
        $method = $matchedControllerAndActionData['_method'];

        $controllerInstance = new $controller($this->request, $this);
        /** @var Response $response */
        $response = $controllerInstance->$method();
        $response->send();
    }

    /**
     * Returns current user
     *
     * @return AppUser
     */
    public function getUser(): AppUser
    {
        return $this->user;
    }
}