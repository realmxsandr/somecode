<?php

namespace AllTools\Core;

class AppUser
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $login;

    /**
     * AppUser constructor.
     * @param int $id
     * @param string $login
     */
    public function __construct(int $id, string $login)
    {
        $this->id = $id;
        $this->login = $login;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }


}