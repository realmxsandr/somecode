<?php

namespace AllTools\Core;

use Symfony\Component\HttpFoundation\Request;

class AuthorizationProvider
{
    /**
     * Returns app user from given $request
     *
     * @param Request $request
     * @return AppUser
     */
    public function getUser(Request $request): AppUser
    {
        return new AppUser(1,'admin');
    }
}