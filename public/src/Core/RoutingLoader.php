<?php


namespace AllTools\Core;


use AllTools\Controller\FixtureController;
use AllTools\Controller\GoodsController;
use AllTools\Controller\OrderController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class RoutingLoader
{
    /** @var RoutingLoader */
    private static $instance = null;
    /** @var Request */
    private $request;
    /** @var RouteCollection */
    private $routeCollection;
    /** @var RequestContext */
    private $context;
    /** @var UrlMatcherInterface */
    private $urlMatcher;

    /**
     * Routing constructor.
     */
    private function __construct()
    {
        $this->context = new RequestContext();
        $this->request = Request::createFromGlobals();
        $this->context->fromRequest($this->request);
        $this->routeCollection = new RouteCollection();
        $this->loadRoutes();
        $this->urlMatcher = new UrlMatcher($this->routeCollection, $this->context);
    }

    /**
     * @return RoutingLoader
     */
    public static function getInstance()
    {
        if (self::$instance == null)
        {
            self::$instance = new RoutingLoader();
        }

        return self::$instance;
    }

    /**
     * Loads all routes
     */
    private function loadRoutes()
    {
        $this->loadApiV1Routes();
    }

    /**
     * Loads API v1 related routes
     */
    private function loadApiV1Routes()
    {
        // Fixtures
        $this->routeCollection->add(
            'load_fixtures',
            new Route('/api/v1/fixtures/load', ['_controller' => FixtureController::class, '_method' => 'loadFixtures'])
        );

        // Goods
        $this->routeCollection->add('goods_get_all',
            $getGoodsRoute = new Route('/api/v1/goods', ['_controller' => GoodsController::class, '_method' => 'getAll'])
        );

        // Order
        $this->routeCollection->add(
            'order_create',
            new Route('/api/v1/order/create', ['_controller' => OrderController::class, '_method' => 'create'])
        );
        $this->routeCollection->add(
            'order_charge',
            new Route('/api/v1/order/charge', ['_controller' => OrderController::class, '_method' => 'charge'])
        );
    }

    /**
     * Returns matched controller and action data related to current request
     * @return  array
     */
    public function getMatchedControllerAndAction(): array
    {
        return $this->urlMatcher->matchRequest($this->request);
    }
}