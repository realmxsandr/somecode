<?php


namespace AllTools\Core;


use AllTools\Repository\GoodsRepository;
use AllTools\Repository\OrderRepository;
use AllTools\Service\OrderCharger\OrderCharger;
use AllTools\Service\OrderCharger\ValidationRule\OrderAmountPaymentAmountEqualsOrderValidationRule;
use AllTools\Service\Payments\BasePaymentGatewayConfiguration;
use AllTools\Service\Payments\YaPaymentGateway\YaPaymentGateway;
use AllTools\Service\Payments\YaPaymentGateway\YaPaymentResponseStatusCodeMapper;
use AllTools\Service\OrderManager\OrderManager;

class ServiceLoader
{
    /** @var ServiceLoader */
    private static $instance = null;
    /** @var object[] */
    private $services;

    /**
     * App constructor.
     */
    private function __construct()
    {
        $this->loadServices();
    }

    /**
     * @return ServiceLoader
     */
    public static function getInstance()
    {
        if (self::$instance == null)
        {
            self::$instance = new ServiceLoader();
        }

        return self::$instance;
    }

    /**
     * Return $classname class instance
     * @param string $classname
     * @return object
     */
    public function getService(string $classname): object
    {
        $result = null;

        if (!isset($this->services[$classname]))
        {
            try{
                $result = new $classname;
            } catch (\Throwable $throwable) {
                throw new \RuntimeException(
                    $throwable->getMessage(). '. ' . $classname . ' instancing must be configured in this App.
                        Configure it in loadService() App class method'
                );
            }
        } else {
            $result = $this->services[$classname];
        }

        return $result;
    }

    /**
     * Definition of services
     */
    private function loadServices()
    {
        // YaPaymentGateway
        $yaPaymentGatewayConfiguration = new BasePaymentGatewayConfiguration();
        $yaPaymentGatewayConfiguration->setBaseUrl('https://ya.ru');
        $yaPaymentGatewayConfiguration->setClientId('someClientId');
        $yaPaymentGatewayConfiguration->setClientPassCode('somePassCode');
        $yaPaymentGateway = new YaPaymentGateway($yaPaymentGatewayConfiguration, new YaPaymentResponseStatusCodeMapper());

        $this->services[YaPaymentGateway::class] = $yaPaymentGateway;
        /** @var YaPaymentGateway $defaultPaymentGateway */
        $defaultPaymentGateway = $this->getService(YaPaymentGateway::class); // Sets YaPaymentGateway as default payment gateway for application

        // OrderCharger
        $orderCharger = new OrderCharger(
            [new OrderAmountPaymentAmountEqualsOrderValidationRule()],
            new OrderRepository(),
            $defaultPaymentGateway
        );
        $this->services[OrderCharger::class] = $orderCharger;

        // OrderManager
        /** @var OrderCharger $orderCharger */
        $orderCharger = $this->getService(OrderCharger::class);
        $orderManager = new OrderManager($orderCharger, new OrderRepository(), new GoodsRepository());
        $this->services[OrderManager::class] = $orderManager;
    }
}