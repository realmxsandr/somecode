<?php

namespace AllTools\DataPersisting\MySQL;

use AllTools\Core\RoutingLoader;

class Connection
{
    /** @var Connection */
    private static $instance = null;
    /** @var \PDO */
    private $pdo;

    private function __construct()
    {
        $dsn = "mysql:host=mysql;dbname=website";
        $user = "web";
        $passwd = "webwebwebweb";

        $this->pdo= new \PDO($dsn, $user, $passwd);
    }

    /**
     * @return Connection
     */
    public static function getInstance()
    {
        if (self::$instance == null)
        {
            self::$instance = new Connection();
        }

        return self::$instance;
    }

    /**
     * Returns \PDOStatement for given $query
     *
     * @param string $query
     * @return bool|\PDOStatement
     */
    public function getPreparedStatement(string $query)
    {
        return $this->pdo->prepare($query);
    }

    /**
     * Return last inserted object id
     *
     * @return int
     */
    public function getLastInsertedId(): int
    {
        return $this->pdo->lastInsertId();
    }

    /**
     * Returns query error data
     *
     * @return array
     */
    public function getLastError(): array
    {
        return $this->pdo->errorInfo();
    }

    /**
     * Begins transaction
     */
    public function beginTransaction(): void
    {
        $this->pdo->beginTransaction();
    }

    /**
     * Commits recently opened transaction
     */
    public function commit(): void
    {
        $this->pdo->commit();
    }
}