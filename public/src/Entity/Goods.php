<?php

namespace AllTools\Entity;

class Goods implements \JsonSerializable
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;
    /** @var float */
    private $cost;

    /**
     * Goods constructor.
     * @param int $id
     * @param string $name
     * @param float $cost
     */
    public function __construct(int $id, string $name, float $cost)
    {
        $this->id = $id;
        $this->name = $name;
        $this->cost = $cost;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getCost(): float
    {
        return $this->cost;
    }

    /**
     * @param float $cost
     */
    public function setCost(float $cost): void
    {
        $this->cost = $cost;
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'cost' => $this->getCost(),
        ];
    }
}