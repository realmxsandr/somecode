<?php

namespace AllTools\Entity;

use AllTools\Core\AppUser;
use AllTools\Service\OrderStates\Exception\OrderStateException;
use AllTools\Service\OrderStates\OrderStateInterface;
use AllTools\Service\OrderStates\OrderStateNew;
use AllTools\Service\OrderStates\OrderStateSimpleFactory;
use AllTools\Service\Payments\PayableInterface;

class Order implements PayableInterface
{
    const STATE_NEW = 1;
    const STATE_CHARGED = 2;
    /** @var int */
    private $id;
    /** @var Goods[] */
    private $items;
    /** @var OrderStateInterface */
    private $state;
    /** @var AppUser  */
    private $user;

    private $statesTransitionMap = [
        OrderStateInterface::STATE_INITIAL  =>  [OrderStateInterface::STATE_NEW, OrderStateInterface::STATE_CHARGED],
        OrderStateInterface::STATE_NEW      => [OrderStateInterface::STATE_NEW, OrderStateInterface::STATE_CHARGED],
        OrderStateInterface::STATE_CHARGED  => [],
    ];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Goods[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param Goods[] $items
     */
    public function setItems(array $items): void
    {
        $this->state->setItems($items);
    }

    /**
     * Used ONLY by Order State
     * Dont use this setter to add items to order, use setItems() method ONLY
     *
     * @deprecated
     * @param Goods[] $items
     */
    public function addItems(array $items): void
    {
        $this->items = $items;
    }

    /**
     * @return OrderStateInterface
     */
    public function getState(): OrderStateInterface
    {
        return $this->state;
    }

    /**
     * @param int $newStateCode
     * @throws OrderStateException
     */
    public function setState(int $newStateCode)
    {
        $currentStateCode = is_null($this->state) ? OrderStateInterface::STATE_INITIAL : $this->state->getStateCode();
        if (in_array($newStateCode, $this->statesTransitionMap[$currentStateCode])) {
            $this->state = OrderStateSimpleFactory::build($newStateCode, $this);
        } else {
            throw OrderStateException::generateInvalidTransitionAttempt($currentStateCode, $newStateCode);
        }
    }

    /**
     * Returns cost of all order items
     *
     * @return float
     */
    public function getAmount()
    {
        $result = .0;

        foreach ($this->items as $item) {
            $result += $item->getCost();
        }

        return $result;
    }

    // Payable interface implementation

    /**
     * {@inheritDoc}
     */
    public function charge(): void {
        $this->state->charge();
    }

    /**
     * {@inheritDoc}
     */
    public function getPayableId(): string
    {
        return $this->getUser()->getId() . '#' . (string) $this->getId();
    }

    /**
     * {@inheritDoc}
     */
    public function getPayableAmount(): float
    {
        return (string) $this->getAmount();
    }

    /**
     * @return AppUser
     */
    public function getUser(): AppUser
    {
        return $this->user;
    }

    /**
     * @param AppUser $user
     */
    public function setUser(AppUser $user): void
    {
        $this->user = $user;
    }


}