<?php

namespace AllTools\Fixtures;

use AllTools\DataPersisting\MySQL\Connection;
use AllTools\Entity\Goods;
use AllTools\Repository\GoodsRepository;
use AllTools\Repository\RepositoryInterface;

class GoodsLoadFixture
{
    /** @var Connection */
    private $connection;
    /** @var RepositoryInterface */
    private $goodsRepository;

    /**
     * GoodsLoadFixture constructor.
     */
    public function __construct()
    {
        $this->connection = Connection::getInstance();
        $this->goodsRepository = new GoodsRepository();
    }


    public function execute()
    {
        $this->purgeDatabase();

        $goods = [
            new Goods(0, 'Suzuki GSX-R 1000 2005', 3000.),
            new Goods(0, 'Suzuki GSX-R 600 2005', 2500.),
            new Goods(0, 'Suzuki GSX-R 750 2005', 2700.),
            new Goods(0, 'Suzuki GSX-R 1000 2003', 2800.),
            new Goods(0, 'Suzuki GSX-R 750  1998', 1800.),
            new Goods(0, 'Suzuki TL1000R 2001', 1800.),
            new Goods(0, 'Suzuki TL100S 1998 2005', 1200.),
            new Goods(0, 'ИЖ Планета-5', 500.),
            new Goods(0, 'Kawasaki ZX10R 2007', 3200.),
            new Goods(0, 'Kawasaki ZX9R 2001', 2600.),
            new Goods(0, 'Kawasaki ZX10 2007', 3100.),
            new Goods(0, 'Suzuki GSX-R 1300 Hayabusa 2003', 3300.),
            new Goods(0, 'Kawasaki H2R 2018', 10500.),
            new Goods(0, 'Kawasaki H2 2018', 10400.),
            new Goods(0, 'Honda 600F4i 2002', 2500.),
            new Goods(0, 'Honda 600RR', 2400.),
            new Goods(0, 'Honda 1000RR Fireblade 2007', 4000.),
            new Goods(0, 'BMW S1000RR 2019', 10500.),
            new Goods(0, 'Yamaha R1 2006', 3200.),
            new Goods(0, 'Yamaha R6 2007', 2900.),
        ];

        $this->goodsRepository->insertBatch($goods);
    }

    /**
     * Purge app database
     */
    private function purgeDatabase(): void
    {
        $clearDatabaseStatement = $this->connection->getPreparedStatement('DELETE FROM `order_query`;');
        $clearDatabaseStatement->execute();
        $clearDatabaseStatement = $this->connection->getPreparedStatement('DELETE FROM `order`;');
        $clearDatabaseStatement->execute();
        $clearDatabaseStatement = $this->connection->getPreparedStatement('DELETE FROM `goods`;');
        $clearDatabaseStatement->execute();
    }
}