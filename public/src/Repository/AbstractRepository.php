<?php

namespace AllTools\Repository;

use AllTools\DataPersisting\MySQL\Connection;
use AllTools\Repository\Exception\RepositoryException;

abstract class AbstractRepository
{
    /** @var Connection */
    protected $connection;
    /** @var string */
    protected static $entityClass;

    /**
     * AbstractRepository constructor.
     */
    public function __construct()
    {
        $this->connection = Connection::getInstance();
    }

    /**
     * Validates that given entity and current repository matches
     *
     * @param object $object
     * @throws RepositoryException
     */
    protected function validateEntityType(object $object): void
    {
        $givenEntityClass = \get_class($object);
        $entityAndRepositoryMatches = static::$entityClass === $givenEntityClass;

        if (!$entityAndRepositoryMatches) {
            throw RepositoryException::generateWrongEntityType($givenEntityClass, \get_class($this), static::$entityClass);
        }
    }
}