<?php

namespace AllTools\Repository\Exception;

class RepositoryException extends \Exception
{
    private const MESSAGE_WRONG_ENTITY = 'Wrong entity class %s for repository &s. Entity class %s expected';

    /**
     * @param $wrongEntityClass
     * @param $repositoryClass
     * @param $expectedEntityClass
     * @return RepositoryException
     */
    public static function generateWrongEntityType($wrongEntityClass, $repositoryClass, $expectedEntityClass)
    {
        return new RepositoryException(sprintf(self::MESSAGE_WRONG_ENTITY, $wrongEntityClass, $repositoryClass, $expectedEntityClass));
    }
}