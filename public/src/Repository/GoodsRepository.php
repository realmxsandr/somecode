<?php

namespace AllTools\Repository;

use AllTools\Entity\Goods;
use AllTools\Entity\Order;
use AllTools\Repository\Exception\RepositoryException;

class GoodsRepository extends AbstractRepository implements ReaderRepositoryInterface, WriterRepositoryInterface
{
    /** @var string */
    protected static $entityClass = Goods::class;
    /**
     * {@inheritDoc}
     */
    public function insert(object $object): void
    {
        $this->validateEntityType($object);
        /** @var Goods $object */

        $this->connection->beginTransaction();
        $insertQuery = '
            INSERT INTO
                `goods` (`name`, `cost`)
            VALUES
                (?, ?);
        ';
        $insertStatement = $this->connection->getPreparedStatement($insertQuery);
        $insertStatement->bindValue(1, $object->getName(), \PDO::PARAM_STR);
        $insertStatement->bindValue(2, $object->getCost(), \PDO::PARAM_INT);
        $this->connection->commit();
        $insertStatement->execute();

        $object->setId($this->connection->getLastInsertedId());
    }

    /**
     * {@inheritDoc}
     */
    public function insertBatch(array $objects): void
    {
        foreach ($objects as $key => $object) {
            $objects[$key] = $this->insert($object);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function update(object $object): void
    {
        $this->validateEntityType($object);
        /** @var Goods $object */

        $this->connection->beginTransaction();
        $updateQuery = '
            UPDATE
                `goods`
            SET
                `name` = ?, `cost` = ?
            WHERE
                id = ?;
        ';
        $updateStatement = $this->connection->getPreparedStatement($updateQuery);
        $updateStatement->bindValue(1, $object->getName(), \PDO::PARAM_INT);
        $updateStatement->bindValue(2, $object->getCost(), \PDO::PARAM_INT);
        $updateStatement->bindValue(3, $object->getId(), \PDO::PARAM_INT);
        $this->connection->commit();
        $updateStatement->execute();
    }

    /**
     * {@inheritDoc}
     */
    public function updateBatch(array $objects): void
    {
        foreach ($objects as $object) {
            $this->update($object);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function get(int $id): ?object
    {
        $result = null;

        $selectQuery = '
            SELECT
                *
            FROM
                `goods`
            WHERE
                `id` = ?
        ';

        $selectStatement = $this->connection->getPreparedStatement($selectQuery);
        $selectStatement->bindValue(1, $id, \PDO::PARAM_INT);
        $selectStatement->execute();
        $fetchedGoodsData = $selectStatement->fetch(\PDO::FETCH_ASSOC);
        if (false !== $fetchedGoodsData) {
            $result = new Goods($fetchedGoodsData['id'], $fetchedGoodsData['name'], $fetchedGoodsData['cost']);
        }

        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function getBatch($ids): \Generator
    {
        foreach ($ids as $id) {
            yield $this->get($id);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getAll(): \Generator
    {
        $selectQuery = '
            SELECT
                *
            FROM
                `goods`;
        ';

        $selectStatement = $this->connection->getPreparedStatement($selectQuery);
        $selectStatement->execute();
        foreach ($selectStatement->fetchAll(\PDO::FETCH_ASSOC) as $fetchedGoodsData) {
            yield new Goods($fetchedGoodsData['id'], $fetchedGoodsData['name'], $fetchedGoodsData['cost']);
        }
    }


}