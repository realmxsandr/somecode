<?php


namespace AllTools\Repository;


use AllTools\Core\App;
use AllTools\Entity\Goods;
use AllTools\Entity\Order;
use AllTools\Repository\Exception\RepositoryException;
use AllTools\Service\OrderStates\Exception\OrderStateException;
use AllTools\Service\OrderStates\OrderStateInterface;

class OrderRepository extends AbstractRepository implements WriterRepositoryInterface, ReaderRepositoryInterface
{
    /** @var string */
    protected static $entityClass = Order::class;

    /**
     * {@inheritDoc}
     */
    public function insert(object $object): void
    {
        $this->validateEntityType($object);
        /** @var Order $object */

        // Inserting the order
        $this->connection->beginTransaction();
        $insertQuery = '
            INSERT INTO
                `order` (`user_id`, `state`)
            VALUES
                (?, ?);
        ';
        $insertStatement = $this->connection->getPreparedStatement($insertQuery);
        $insertStatement->bindValue(1, $object->getUser()->getId(), \PDO::PARAM_INT);
        $insertStatement->bindValue(2, $object->getState()->getStateCode(), \PDO::PARAM_INT);
        $this->connection->commit();
        $insertStatement->execute();

        $object->setId($this->connection->getLastInsertedId());

        // Inserting order items
        $this->connection->beginTransaction();
        $relateGoodsToOrderQuery = '
            INSERT INTO
                `order_goods` (`order_id`, `goods_id`)
            VALUES         
        ';
        /** @var Goods $item */
        foreach ($object->getItems() as $item) {
            $relateGoodsToOrderQuery .= '(?, ?), ';
        }
        $relateGoodsToOrderQuery = mb_substr($relateGoodsToOrderQuery, 0 , -2) . ';';

        $relateGoodsToOrderStatement = $this->connection->getPreparedStatement($relateGoodsToOrderQuery);
        /** @var Goods $item */
        $parameterCounter = 0;
        foreach ($object->getItems() as $item) {
            $relateGoodsToOrderStatement->bindValue(++$parameterCounter, $object->getId());
            $relateGoodsToOrderStatement->bindValue(++$parameterCounter, $item->getId());
        }
        $this->connection->commit();
        $relateGoodsToOrderStatement->execute();
    }

    /**
     * {@inheritDoc}
     */
    public function insertBatch(array $objects): void
    {
        foreach ($objects as $key => $object) {
            $objects[$key] = $this->insert($object);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function update(object $object): void
    {
        $this->validateEntityType($object);
        /** @var Order $object */

        // Removing old order items
        $deleteOrderItemsQuery = '
            DELETE FROM
                `order_goods`
            WHERE
                `order_id` = ?
        ';
        $deleteOrderItemsStatement = $this->connection->getPreparedStatement($deleteOrderItemsQuery);
        $deleteOrderItemsStatement->bindValue(1, $object->getId(), \PDO::PARAM_INT);
        $deleteOrderItemsStatement->execute();

        // Saving new order items
        $this->connection->beginTransaction();
        $relateGoodsToOrderQuery = '
            INSERT INTO
                `order_goods` (`order_id`, `goods_id`)
            VALUES         
        ';
        /** @var Goods $item */
        foreach ($object->getItems() as $item) {
            $relateGoodsToOrderQuery .= '(?, ?), ';
        }
        $relateGoodsToOrderQuery = mb_substr($relateGoodsToOrderQuery, 0 , -2) . ';';

        $relateGoodsToOrderStatement = $this->connection->getPreparedStatement($relateGoodsToOrderQuery);
        /** @var Goods $item */
        $parameterCounter = 0;
        foreach ($object->getItems() as $item) {
            $relateGoodsToOrderStatement->bindValue(++$parameterCounter, $object->getId());
            $relateGoodsToOrderStatement->bindValue(++$parameterCounter, $item->getId());
        }
        $this->connection->commit();
        $relateGoodsToOrderStatement->execute();

        // Updating the order
        $this->connection->beginTransaction();
        $insertQuery = '
            UPDATE
                `order` 
            SET
                `user_id` = ?, `state` = ?
            WHERE
                `id` = ?;
        ';
        $insertStatement = $this->connection->getPreparedStatement($insertQuery);
        $insertStatement->bindValue(1, $object->getUser()->getId(), \PDO::PARAM_INT);
        $insertStatement->bindValue(2, $object->getState()->getStateCode(), \PDO::PARAM_INT);
        $insertStatement->bindValue(3, $object->getId(), \PDO::PARAM_INT);
        $this->connection->commit();
        $insertStatement->execute();
    }

    /**
     * {@inheritDoc}
     */
    public function updateBatch(array $objects): void
    {
        foreach ($objects as $object) {
            $this->update($object);
        }
    }

    /**
     * {@inheritDoc}
     * @throws OrderStateException
     */
    public function get(int $id): ?object
    {
        $app = App::getInstance();
        $result = null;

        // Getting the order
        $selectQuery = '
            SELECT
                *
            FROM
                `order`
            WHERE
                `id` = ?
        ';
        $selectStatement = $this->connection->getPreparedStatement($selectQuery);
        $selectStatement->bindValue(1, $id, \PDO::PARAM_INT);
        $selectStatement->execute();
        $fetchedOrderData = $selectStatement->fetch(\PDO::FETCH_ASSOC);

        if (false !== $fetchedOrderData) {
            // Getting order items
            $orderItemsQuery = '
                SELECT
                    `goods_id`
                FROM
                    `order_goods`
                WHERE
                    `order_id` = ?;
            ';
            $orderItemsStatement = $this->connection->getPreparedStatement($orderItemsQuery);
            $orderItemsStatement->bindValue(1,$fetchedOrderData['id']);
            $orderItemsStatement->execute();
            $orderItemsData = $orderItemsStatement->fetchAll(\PDO::FETCH_ASSOC);
            $orderItemsIds = [];
            foreach ($orderItemsData as $goods) {
                $orderItemsIds[] = $goods['goods_id'];
            }

            // Fill rest of the order data
            $result = new Order();
            $result->setId($fetchedOrderData['id']);
            $result->setUser($app->getUser());
            $result->setState(OrderStateInterface::STATE_NEW);
            $result->setItems(\iterator_to_array((new GoodsRepository())->getBatch($orderItemsIds)));
            $result->setState($fetchedOrderData['state']);
        }

        return $result;
    }

    /**
     * {@inheritDoc}
     * @throws OrderStateException
     */
    public function getBatch($ids): \Generator
    {
        $result = [];

        foreach ($ids as $id) {
            yield $this->get($id);
        }

        return $result;
    }

    /**
     * {@inheritDoc}
     * @throws OrderStateException
     */
    public function getAll(): \Generator
    {
        $selectQuery = '
            SELECT
                *
            FROM
                `order`;
        ';
        $selectStatement = $this->connection->getPreparedStatement($selectQuery);
        $selectStatement->execute();
        $fetchedOrdersData = $selectStatement->fetch(\PDO::FETCH_ASSOC);

        foreach ($fetchedOrdersData as $order)
        {
            yield $this->get($order['id']);
        }
    }
}