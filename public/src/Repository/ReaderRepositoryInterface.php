<?php

namespace AllTools\Repository;

interface ReaderRepositoryInterface
{
    /**
     * Fetch object from database
     *
     * @param int $id
     * @return object|null
     */
    public function get(int $id): ?object;

    /**
     * Fetch objects with specified $ids from database
     *
     * @return \Generator
     */
    public function getBatch($ids): \Generator;

    /**
     * Fetch objects from database
     *
     * @return \Generator
     */
    public function getAll(): \Generator;
}