<?php

namespace AllTools\Repository;

use AllTools\Repository\Exception\RepositoryException;

interface WriterRepositoryInterface
{
    /**
     * Inserts a new object
     *
     * @param object $object
     * @return void
     * @throws RepositoryException
     *
     */
    public function insert(object $object): void ;

    /**
     * Inserts a batch of new objects
     *
     * @param object[] $objects
     * @return void
     * @throws RepositoryException
     */
    public function insertBatch(array $objects): void;

    /**
     * Updates a object
     *
     * @param object $object
     * @return void
     * @throws RepositoryException
     */
    public function update(object $object): void;

    /**
     * Updates objects
     *
     * @param object[] $objects
     * @return void
     * @throws RepositoryException
     */
    public function updateBatch(array $objects): void;
}