<?php

namespace AllTools\Service\OrderCharger;

use AllTools\Entity\Order;
use AllTools\Repository\Exception\RepositoryException;
use AllTools\Repository\OrderRepository;
use AllTools\Repository\WriterRepositoryInterface;
use AllTools\Service\OrderCharger\ValidationRule\OrderValidationRuleInterface;
use AllTools\Service\OrderCharger\ValidationRule\ValidationRuleParameterBag;
use AllTools\Service\OrderStates\Exception\OrderStateException;
use AllTools\Service\OrderStates\OrderStateInterface;
use AllTools\Service\Payments\BasePaymentRequest;
use AllTools\Service\Payments\PaymentGatewayInterface;
use AllTools\Service\Payments\PaymentResponseInterface;

class OrderCharger
{

    /** @var OrderValidationRuleInterface[]  */
    private $orderValidationRules;
    /** @var WriterRepositoryInterface */
    private $orderRepository;
    /** @var PaymentGatewayInterface */
    private $defaultPaymentGateway;

    /**
     * OrderCharger constructor.
     * @param array $orderValidationRules
     * @param OrderRepository $orderRepository
     */
    public function __construct(
        array $orderValidationRules,
        OrderRepository $orderRepository,
        PaymentGatewayInterface $paymentGateway
    )
    {
        $this->orderValidationRules = $orderValidationRules;
        $this->orderRepository = $orderRepository;
        $this->defaultPaymentGateway = $paymentGateway;
    }

    /**
     * Charge $order using $paymentGateway
     *
     * @param Order $order
     * @param float $amount
     * @return PaymentResponseInterface
     * @throws OrderStateException
     * @throws RepositoryException
     */
    public function charge(Order $order, float $amount): PaymentResponseInterface
    {
        if(OrderStateInterface::STATE_CHARGED === $order->getState()->getStateCode()) {
            throw OrderStateException::generateAlreadyCharged($order);
        }
        $validationRuleParameterBag = new ValidationRuleParameterBag();
        $validationRuleParameterBag->setPaymentAmount($amount);
        $this->validatePayment($order, $validationRuleParameterBag);

        $paymentRequest = new BasePaymentRequest();
        $paymentRequest->setPayable($order);

        $paymentGatewayResponse = $this->defaultPaymentGateway->charge($paymentRequest);

        $order->charge();
        $this->orderRepository->update($order);

        return $paymentGatewayResponse;
    }

    /**
     * @param PaymentGatewayInterface $defaultPaymentGateway
     */
    public function setDefaultPaymentGateway(PaymentGatewayInterface $defaultPaymentGateway): void
    {
        $this->defaultPaymentGateway = $defaultPaymentGateway;
    }

    /**
     * Validate order against charger validation rules
     *
     * @param Order $order
     * @param ValidationRuleParameterBag $validationRuleParameterBag
     */
    private function validatePayment(Order $order, ValidationRuleParameterBag $validationRuleParameterBag)
    {
        foreach ($this->orderValidationRules as $validationRule) {
            $validationRule->validate($order, $validationRuleParameterBag);
        }
    }
}