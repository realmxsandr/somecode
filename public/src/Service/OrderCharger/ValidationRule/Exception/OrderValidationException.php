<?php

namespace AllTools\Service\OrderCharger\ValidationRule\Exception;

class OrderValidationException extends \Exception
{
    private const MESSAGE_ORDER_AMOUNT_PAYMENT_AMOUNT_DOESNT_MATCH = 'Order amount %s and payment amount %s doesnt match';

    /**
     * @param string $orderAmount
     * @param string $paymentAmount
     * @return OrderValidationException
     */
    public static function generateOrderPaymentAmountsDoesntMatch(string $orderAmount, string $paymentAmount)
    {
        return new OrderValidationException(sprintf(
            self::MESSAGE_ORDER_AMOUNT_PAYMENT_AMOUNT_DOESNT_MATCH, $orderAmount, $paymentAmount));
    }
}