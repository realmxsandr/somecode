<?php

namespace AllTools\Service\OrderCharger\ValidationRule;

use AllTools\Entity\Order;
use AllTools\Service\OrderCharger\ValidationRule\Exception\OrderValidationException;

class OrderAmountPaymentAmountEqualsOrderValidationRule implements OrderValidationRuleInterface
{
    /**
     * {@inheritDoc}
     * @throws OrderValidationException
     */
    public function validate(Order $order, ValidationRuleParameterBag $validationRuleParameterBag): bool
    {
        $orderAmount = $order->getAmount();
        $paymentAmount = $validationRuleParameterBag->getPaymentAmount();
        $ruleIsValid = $orderAmount === $paymentAmount;

        if ($ruleIsValid) {
            return true;
        } else {
            throw OrderValidationException::generateOrderPaymentAmountsDoesntMatch((string)$orderAmount, (string)$paymentAmount);
        }
    }
}