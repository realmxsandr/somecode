<?php

namespace AllTools\Service\OrderCharger\ValidationRule;

use AllTools\Entity\Order;

interface OrderValidationRuleInterface
{
    /**
     * Validates order according to rule
     * @param Order $order
     * @param ValidationRuleParameterBag $validationRuleParameterBag
     * @return mixed
     */
    public function validate(Order $order, ValidationRuleParameterBag $validationRuleParameterBag);
}