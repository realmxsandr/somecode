<?php

namespace AllTools\Service\OrderCharger\ValidationRule;

class ValidationRuleParameterBag
{
    /** @var float */
    private $paymentAmount;

    /**
     * @return float
     */
    public function getPaymentAmount(): float
    {
        return $this->paymentAmount;
    }

    /**
     * @param float $paymentAmount
     */
    public function setPaymentAmount(float $paymentAmount): void
    {
        $this->paymentAmount = $paymentAmount;
    }
}