<?php

namespace AllTools\Service\OrderManager\Exception;

class OrderManagerException extends \Exception
{
    private const MESSAGE_NOT_DISTINCT_GOODS = 'Order goods must not repeat in one order';
    private const MESSAGE_INVALID_GOODS = 'Some request goods are not present in the system';
    private const MESSAGE_UNKNOWN_ORDER = 'Order with provided ID does not exists';

    /**
     * @return OrderManagerException
     */
    public static function generateNotDistinctGoods()
    {
        return new OrderManagerException(self::MESSAGE_NOT_DISTINCT_GOODS);
    }

    /**
     * @return OrderManagerException
     */
    public static function generateInvalidGoodsInOrder()
    {
        return new OrderManagerException(self::MESSAGE_INVALID_GOODS);
    }

    /**
     * @return OrderManagerException
     */
    public static function generateUnknownOrder()
    {
        return new OrderManagerException(self::MESSAGE_UNKNOWN_ORDER);
    }
}