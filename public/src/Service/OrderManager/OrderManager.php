<?php

namespace AllTools\Service\OrderManager;

use AllTools\Core\AppUser;
use AllTools\Entity\Goods;
use AllTools\Entity\Order;
use AllTools\Repository\Exception\RepositoryException;
use AllTools\Repository\ReaderRepositoryInterface;
use AllTools\Repository\WriterRepositoryInterface;
use AllTools\Service\OrderCharger\OrderCharger;
use AllTools\Service\OrderStates\Exception\OrderStateException;
use AllTools\Service\OrderStates\OrderStateInterface;
use AllTools\Service\OrderManager\Exception\OrderManagerException;
use AllTools\Service\Payments\PaymentGatewayInterface;
use AllTools\Service\Payments\PaymentResponseInterface;

class OrderManager
{
    /** @var OrderCharger */
    private $orderCharger;
    /** @var WriterRepositoryInterface */
    private $orderRepository;
    /** @var ReaderRepositoryInterface */
    private $goodsRepository;
    /** @var PaymentGatewayInterface */
    private $paymentGateway;

    /**
     * OrderManager constructor.
     * @param OrderCharger $orderCharger
     * @param WriterRepositoryInterface $orderRepository
     * @param ReaderRepositoryInterface $goodsRepository
     */
    public function __construct(
        OrderCharger $orderCharger,
        WriterRepositoryInterface $orderRepository,
        ReaderRepositoryInterface $goodsRepository
    )
    {
        $this->orderCharger = $orderCharger;
        $this->orderRepository = $orderRepository;
        $this->goodsRepository = $goodsRepository;
    }

    /**
     * Creates a order for $user with goods with $withGoodsIds ids
     *
     * @param AppUser $forUser
     * @param array $withGoodsIds
     * @return Order
     * @throws OrderStateException
     * @throws RepositoryException
     * @throws OrderManagerException
     */
    public function createOrder(AppUser $forUser, array $withGoodsIds): Order
    {
        $this->validateNewOrderGoodsAreDistinct($withGoodsIds);

        $order = new Order();
        $orderItems = \iterator_to_array($this->goodsRepository->getBatch($withGoodsIds));
        $this->validateNewOrderAllGoodsAreExists($orderItems);

        $order->setUser($forUser);
        $order->setState(OrderStateInterface::STATE_NEW);
        $order->setItems($orderItems);

        $this->orderRepository->insert($order);

        return $order;
    }

    /**
     * Charge $order with amount $withAmount
     *
     * @param Order $order
     * @param float $withAmount
     * @return PaymentResponseInterface
     * @throws OrderStateException
     * @throws RepositoryException
     */
    public function chargeOrder(Order $order, float $withAmount): PaymentResponseInterface
    {
        return $this->orderCharger->charge($order, $withAmount);
    }

    /**
     * Make sure that new order goods are not repeats
     * @param array $newOrderGoodsIds
     * @return void
     * @throws OrderManagerException
     */
    private function validateNewOrderGoodsAreDistinct(array $newOrderGoodsIds): void
    {
        $newOrderGoodsAreDistinct = \count($newOrderGoodsIds) === \count(\array_unique($newOrderGoodsIds));

        if (!$newOrderGoodsAreDistinct) {
            throw OrderManagerException::generateNotDistinctGoods();
        }
    }

    /**
     * Make sure that all goods in new order are exists
     *
     * @param array $newOrderGoods
     * @throws OrderManagerException
     */
    private function validateNewOrderAllGoodsAreExists(array $newOrderGoods): void
    {
        foreach ($newOrderGoods as $goods) {
            if (!($goods instanceof Goods)) {
                throw OrderManagerException::generateInvalidGoodsInOrder();
            }
        }
    }
}