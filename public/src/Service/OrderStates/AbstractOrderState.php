<?php

namespace AllTools\Service\OrderStates;

use AllTools\Entity\Order;

abstract class AbstractOrderState implements OrderStateInterface
{
    /** @var int  */
    protected static $stateCode;
    /** @var Order */
    protected $order;

    /**
     * AbstractOrderState constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * {@inheritDoc}
     */
    public function getStateCode(): int
    {
        return static::$stateCode;
    }
}