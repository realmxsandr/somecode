<?php

namespace AllTools\Service\OrderStates\Exception;

use AllTools\Entity\Order;

class OrderStateException extends \Exception
{
    private const MESSAGE_ALREADY_CHARGED = 'Order #%s already charged';
    private const MESSAGE_CHARGED_ORDERS_MUST_BE_IMMUTABLE = 'Items can not be added to charged order';
    private const MESSAGE_UNEXPECTED_STATUS = 'Undefined status code: %s';
    private const MESSAGE_STATE_TRANSITION_NOT_ALLOWED = 'Transition from status with code %s to status with code %s is not allowed';

    /**
     * @param Order $order
     * @return OrderStateException
     */
    public static function generateAlreadyCharged(Order $order) {
        return new OrderStateException(sprintf(self::MESSAGE_ALREADY_CHARGED, (string) $order->getId()));
    }

    /**
     * @return OrderStateException
     */
    public static function generateChargedOrderModificationAttempt() {
        return new OrderStateException(self::MESSAGE_CHARGED_ORDERS_MUST_BE_IMMUTABLE);
    }

    /**
     * @param int $statusCode
     * @return OrderStateException
     */
    public static function generateUndefinedStatusCode(int $statusCode) {
        return new OrderStateException(sprintf(self::MESSAGE_UNEXPECTED_STATUS, (string) $statusCode));
    }

    /**
     * @param int $fromCode
     * @param int $toCode
     * @return OrderStateException
     */
    public static function generateInvalidTransitionAttempt(int $fromCode, int $toCode) {
        return new OrderStateException(sprintf(self::MESSAGE_STATE_TRANSITION_NOT_ALLOWED, (string)$fromCode, (string)$toCode));
    }
}