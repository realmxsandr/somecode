<?php

namespace AllTools\Service\OrderStates;


use AllTools\Service\OrderStates\Exception\OrderStateException;

class OrderStateCharged extends AbstractOrderState
{
    /** @var int  */
    protected static $stateCode = OrderStateInterface::STATE_CHARGED;

    /**
     * {@inheritDoc}
     * @throws OrderStateException
     */
    public function charge(): void
    {
        throw OrderStateException::generateAlreadyCharged($this->order);
    }

    /**
     * {@inheritDoc}
     * @throws OrderStateException
     */
    public function setItems(array $items): void
    {
        throw OrderStateException::generateChargedOrderModificationAttempt();
    }
}