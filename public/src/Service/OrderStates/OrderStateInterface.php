<?php

namespace AllTools\Service\OrderStates;

use AllTools\Entity\Goods;

interface OrderStateInterface
{
    const STATE_INITIAL = 0;
    const STATE_NEW = 1;
    const STATE_CHARGED = 2;

    /**
     * Make order charged
     *
     * @return void
     */
    public function charge(): void;

    /**
     * Sets order items
     *
     * @param Goods[] $items
     */
    public function setItems(array $items): void;

    /**
     * Returns state code
     *
     * @return int
     */
    public function getStateCode(): int;
}