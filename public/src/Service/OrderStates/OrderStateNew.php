<?php

namespace AllTools\Service\OrderStates;

use AllTools\Service\OrderStates\Exception\OrderStateException;

class OrderStateNew extends AbstractOrderState
{
    /** @var int  */
    protected static $stateCode = OrderStateInterface::STATE_NEW;

    /**
     * {@inheritDoc}
     * @throws OrderStateException
     */
    public function charge(): void
    {
        $this->order->setState(self::STATE_CHARGED);
    }

    /**
     * {@inheritDoc}
     */
    public function setItems(array $items): void
    {
        $this->order->addItems($items);
    }
}