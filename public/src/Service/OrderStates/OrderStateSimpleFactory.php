<?php

namespace AllTools\Service\OrderStates;

use AllTools\Entity\Order;
use AllTools\Service\OrderStates\Exception\OrderStateException;

class OrderStateSimpleFactory
{
    /**
     * Creates a OrderStateInterface implementation based on given $statusCode for $order
     *
     * @param int $statusCode
     * @param Order $order
     * @return OrderStateInterface
     * @throws OrderStateException
     */
    public static function build(int $statusCode, Order $order): OrderStateInterface
    {
        switch ($statusCode) {
            case OrderStateInterface::STATE_NEW:
                return new OrderStateNew($order);
            case OrderStateInterface::STATE_CHARGED:
                return new OrderStateCharged($order);
            default:
                throw OrderStateException::generateUndefinedStatusCode($statusCode);
        }
    }
}