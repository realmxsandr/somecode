<?php

namespace AllTools\Service\Payments;

class BasePaymentGatewayConfiguration implements PaymentGatewayConfigurationInterface
{
    /** @var string */
    private $clientId;
    /** @var string|null  */
    private $customLogin;
    /** @var string */
    private $clientPassCode;
    /** @var string|null */
    private $SSLCertificatePath;
    /** @var string */
    private $baseUrl;

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    public function setClientId(string $clientId): void
    {
        $this->clientId = $clientId;
    }

    /**
     * @return string|null
     */
    public function getCustomLogin(): ?string
    {
        return $this->customLogin;
    }

    /**
     * @param string|null $customLogin
     */
    public function setCustomLogin(?string $customLogin): void
    {
        $this->customLogin = $customLogin;
    }

    /**
     * @return string
     */
    public function getClientPassCode(): string
    {
        return $this->clientPassCode;
    }

    /**
     * @param string $clientPassCode
     */
    public function setClientPassCode(string $clientPassCode): void
    {
        $this->clientPassCode = $clientPassCode;
    }

    /**
     * @return string|null
     */
    public function getSSLCertificatePath(): ?string
    {
        return $this->SSLCertificatePath;
    }

    /**
     * @param string|null $SSLCertificatePath
     */
    public function setSSLCertificatePath(?string $SSLCertificatePath): void
    {
        $this->SSLCertificatePath = $SSLCertificatePath;
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * @param string $baseUrl
     */
    public function setBaseUrl(string $baseUrl): void
    {
        $this->baseUrl = $baseUrl;
    }
}