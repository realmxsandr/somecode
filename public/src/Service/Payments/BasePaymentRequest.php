<?php

namespace AllTools\Service\Payments;

class BasePaymentRequest implements PaymentRequestInterface
{
    /** @var PayableInterface */
    private $payable;

    /**
     * @return PayableInterface
     */
    public function getPayable(): PayableInterface
    {
        return $this->payable;
    }

    /**
     * @param PayableInterface $payable
     */
    public function setPayable(PayableInterface $payable): void
    {
        $this->payable = $payable;
    }
}