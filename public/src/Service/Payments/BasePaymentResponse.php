<?php

namespace AllTools\Service\Payments;

class BasePaymentResponse implements PaymentResponseInterface
{
    /** @var int */
    private $status;
    /** @var string|null */
    private $message;

    /**
     * BasePaymentResponse constructor.
     * @param int $status
     * @param string|null $message
     */
    public function __construct(int $status, ?string $message)
    {
        $this->status = $status;
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     */
    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }
}