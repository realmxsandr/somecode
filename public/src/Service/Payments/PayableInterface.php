<?php

namespace AllTools\Service\Payments;

interface PayableInterface
{
    /**
     * Abstract payable external id
     * @return string
     */
    public function getPayableId(): string;

    /**
     * Amount to pay
     * @return float
     */
    public function getPayableAmount(): float;

    /**
     * Charge the payable
     * @return void
     */
    public function charge(): void;
}