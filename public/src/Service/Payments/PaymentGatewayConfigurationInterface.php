<?php


namespace AllTools\Service\Payments;


interface PaymentGatewayConfigurationInterface
{
    /**
     * App ID
     * @return string
     */
    public function getClientId(): string;
    /**
     * Custom identification property
     * @return string|null
     */
    public function getCustomLogin(): ?string;
    /**
     * App pass code
     * @return string
     */
    public function getClientPassCode(): string;
    /**
     * App ID
     * @return string|null
     */
    public function getSSLCertificatePath(): ?string ;

    /**
     * Returns base payment gateway interface
     *
     * @return string
     */
    public function getBaseUrl(): string;
}