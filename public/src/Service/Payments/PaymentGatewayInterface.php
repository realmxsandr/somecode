<?php

namespace AllTools\Service\Payments;

interface PaymentGatewayInterface
{
    /**
     * Charge the $paymentRequest
     * @param PaymentRequestInterface $paymentRequest
     * @return PaymentResponseInterface
     */
    public function charge(PaymentRequestInterface $paymentRequest): PaymentResponseInterface;
}