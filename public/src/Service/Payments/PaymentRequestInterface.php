<?php

namespace AllTools\Service\Payments;

interface PaymentRequestInterface
{
    /**
     * Get the payable item
     * @return PayableInterface
     */
    public function getPayable(): PayableInterface;
}