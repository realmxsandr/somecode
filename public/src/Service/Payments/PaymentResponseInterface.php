<?php


namespace AllTools\Service\Payments;


interface PaymentResponseInterface
{
    const STATUS_OK = 0;
    const STATUS_ERROR = 1;

    /**
     * Response status code, value of STATUS_ constants
     * @return int
     */
    public function getStatus(): int;

    /**
     * A response message, maybe error explanation, etc.
     * @return string|null
     */
    public function getMessage(): ?string;
}