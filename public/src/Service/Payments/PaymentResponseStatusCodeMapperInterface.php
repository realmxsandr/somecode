<?php

namespace AllTools\Service\Payments;

interface PaymentResponseStatusCodeMapperInterface
{
    /**
     * Map a payment gateway $externalResponseStatusCode to internal status code enumerated in AllTools\Service\Payments\PaymentResponseInterface
     * @param string $externalResponseStatusCode
     * @return string
     */
    public function map(string $externalResponseStatusCode): string;
}