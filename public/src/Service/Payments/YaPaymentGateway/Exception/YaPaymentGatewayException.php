<?php

namespace AllTools\Service\Payments\YaPaymentGateway\Exception;

class YaPaymentGatewayException extends \Exception
{
    private const MESSAGE_CURL_ERROR = 'cURL error occurred during charge attempt: %s';

    public static function generateCurlError(string $cURLError)
    {
        return new YaPaymentGatewayException(sprintf(self::MESSAGE_CURL_ERROR, $cURLError));
    }
}