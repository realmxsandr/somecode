<?php

namespace AllTools\Service\Payments\YaPaymentGateway;

use AllTools\Service\Payments\BasePaymentResponse;
use AllTools\Service\Payments\PaymentGatewayConfigurationInterface;
use AllTools\Service\Payments\PaymentGatewayInterface;
use AllTools\Service\Payments\PaymentRequestInterface;
use AllTools\Service\Payments\PaymentResponseStatusCodeMapperInterface;
use AllTools\Service\Payments\PaymentResponseInterface;
use AllTools\Service\Payments\YaPaymentGateway\Exception\YaPaymentGatewayException;

class YaPaymentGateway implements PaymentGatewayInterface
{
    /** @var PaymentGatewayConfigurationInterface */
    private $configuration;
    /** @var PaymentResponseStatusCodeMapperInterface */
    private $responseCodeMapper;

    /**
     * YaPaymentGateway constructor.
     * @param PaymentGatewayConfigurationInterface $configuration
     * @param PaymentResponseStatusCodeMapperInterface $responseCodeMapper
     */
    public function __construct(PaymentGatewayConfigurationInterface $configuration, PaymentResponseStatusCodeMapperInterface $responseCodeMapper)
    {
        $this->configuration = $configuration;
        $this->responseCodeMapper = $responseCodeMapper;
    }

    /**
     * {@inheritDoc}
     */
    public function charge(PaymentRequestInterface $paymentRequest): PaymentResponseInterface
    {
        try {
            $ch = \curl_init();

            \curl_setopt($ch, CURLOPT_URL, $this->configuration->getBaseUrl()); //Url together with parameters
            \curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 7); //Timeout after 7 seconds
            \curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            \curl_setopt($ch, CURLOPT_HEADER, 0);
            \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $curlExecutionSucceed = \curl_exec($ch) !== false;
            $curlExecutionResultHTTPCode = \curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
            $curlExecutionError = \curl_error($ch);
            \curl_close($ch);

            if ($curlExecutionSucceed) {
                $response = new BasePaymentResponse($this->responseCodeMapper->map((string)$curlExecutionResultHTTPCode), null);
            } else {
                throw YaPaymentGatewayException::generateCurlError($curlExecutionError);
            }
        } catch (\Throwable $throwable) {
            $response = new BasePaymentResponse(PaymentResponseInterface::STATUS_ERROR, $throwable->getMessage());
        }

        return $response;
    }
}