<?php

namespace AllTools\Service\Payments\YaPaymentGateway;

use AllTools\Service\Payments\PaymentResponseInterface;
use AllTools\Service\Payments\PaymentResponseStatusCodeMapperInterface;
use Symfony\Component\HttpFoundation\Response;

class YaPaymentResponseStatusCodeMapper implements PaymentResponseStatusCodeMapperInterface
{
    const YA_STATUS_OK_1 = Response::HTTP_OK;

    /**
     * {@inheritDoc}
     */
    public function map(string $externalResponseStatusCode): string
    {
        if((string) self::YA_STATUS_OK_1 === $externalResponseStatusCode) {
            return PaymentResponseInterface::STATUS_OK;
        } else {
            return PaymentResponseInterface::STATUS_ERROR;
        }
    }
}