<?php

namespace AllTools\Tests\Service\OrderCharger;

use AllTools\Core\App;
use AllTools\Core\AppUser;
use AllTools\Entity\Goods;
use AllTools\Entity\Order;
use AllTools\Service\OrderCharger\OrderCharger;
use AllTools\Service\OrderCharger\ValidationRule\Exception\OrderValidationException;
use AllTools\Service\OrderCharger\ValidationRule\OrderAmountPaymentAmountEqualsOrderValidationRule;
use AllTools\Service\OrderStates\OrderStateInterface;
use AllTools\Service\Payments\PaymentResponseInterface;
use AllTools\Service\Payments\YaPaymentGateway\YaPaymentGateway;
use PHPUnit\Framework\TestCase;

class OrderChargerTest extends TestCase
{
    /**
     * @test
     */
    public function orderAmountPaymentAmountEqualsOrderValidationRuleTest()
    {
        $app = App::getInstance();
        /** @var OrderCharger $orderCharger */
        $orderCharger = $app->getService(OrderCharger::class);
        /** @var YaPaymentGateway $yaPaymentGateway */
        $yaPaymentGateway = $app->getService(YaPaymentGateway::class);
        $goods1 = new Goods(1, 'Suzuki GSX-R 1000', 4000);
        $goods2 = new Goods(2, 'Kawasaki ZX10R', 4500);

        $order = new Order();
        $order->setId(1);
        $order->setUser(new AppUser(1,'admin'));
        $order->setState(OrderStateInterface::STATE_NEW);
        $order->setItems([$goods1, $goods2]);
        $paymentResult = $orderCharger->charge($order, 8500, $yaPaymentGateway);
        $this->assertEquals($paymentResult->getStatus(),PaymentResponseInterface::STATUS_OK);
    }

    /**
     * @test
     */
    public function orderAmountGreaterPaymentAmountOrderValidationRuleTest()
    {
        $app = App::getInstance();
        /** @var OrderCharger $orderCharger */
        $orderCharger = $app->getService(OrderCharger::class);
        /** @var YaPaymentGateway $yaPaymentGateway */
        $yaPaymentGateway = $app->getService(YaPaymentGateway::class);
        $goods1 = new Goods(1, 'Suzuki GSX-R 1000', 4000);
        $goods2 = new Goods(2, 'Kawasaki ZX10R', 4500);

        $order = new Order();
        $order->setState(OrderStateInterface::STATE_NEW);
        $order->setItems([$goods1, $goods2]);
        try {
            $paymentResult = $orderCharger->charge($order, 9000, $yaPaymentGateway);
        } catch (\Throwable $throwable) {
            $this->assertEquals(OrderValidationException::class, get_class($throwable));
        }
    }

    /**
     * @test
     */
    public function orderAmountLesserPaymentAmountOrderValidationRuleTest()
    {
        $app = App::getInstance();
        /** @var OrderCharger $orderCharger */
        $orderCharger = $app->getService(OrderCharger::class);
        /** @var YaPaymentGateway $yaPaymentGateway */
        $yaPaymentGateway = $app->getService(YaPaymentGateway::class);
        $goods1 = new Goods(1, 'Suzuki GSX-R 1000', 4000);
        $goods2 = new Goods(2, 'Kawasaki ZX10R', 4500);

        $order = new Order();
        $order->setState(OrderStateInterface::STATE_NEW);
        $order->setItems([$goods1, $goods2]);
        try {
            $paymentResult = $orderCharger->charge($order, 10000, $yaPaymentGateway);
        } catch (\Throwable $throwable) {
            $this->assertEquals(OrderValidationException::class, get_class($throwable));
        }
    }
}